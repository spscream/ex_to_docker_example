# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :ex_to_docker_example,
  ecto_repos: [ExToDockerExample.Repo]

# Configures the endpoint
config :ex_to_docker_example, ExToDockerExample.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "2Q5Xzs1n9p8ZExojG6rvw0J1/1XS/VJSqV7K/7N68jxuax9PcwsP045NigENb24U",
  render_errors: [view: ExToDockerExample.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ExToDockerExample.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
