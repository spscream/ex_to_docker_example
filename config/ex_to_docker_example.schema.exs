@moduledoc """
A schema is a keyword list which represents how to map, transform, and validate
configuration values parsed from the .conf file. The following is an explanation of
each key in the schema definition in order of appearance, and how to use them.

## Import

A list of application names (as atoms), which represent apps to load modules from
which you can then reference in your schema definition. This is how you import your
own custom Validator/Transform modules, or general utility modules for use in
validator/transform functions in the schema. For example, if you have an application
`:foo` which contains a custom Transform module, you would add it to your schema like so:

`[ import: [:foo], ..., transforms: ["myapp.some.setting": MyApp.SomeTransform]]`

## Extends

A list of application names (as atoms), which contain schemas that you want to extend
with this schema. By extending a schema, you effectively re-use definitions in the
extended schema. You may also override definitions from the extended schema by redefining them
in the extending schema. You use `:extends` like so:

`[ extends: [:foo], ... ]`

## Mappings

Mappings define how to interpret settings in the .conf when they are translated to
runtime configuration. They also define how the .conf will be generated, things like
documention, @see references, example values, etc.

See the moduledoc for `Conform.Schema.Mapping` for more details.

## Transforms

Transforms are custom functions which are executed to build the value which will be
stored at the path defined by the key. Transforms have access to the current config
state via the `Conform.Conf` module, and can use that to build complex configuration
from a combination of other config values.

See the moduledoc for `Conform.Schema.Transform` for more details and examples.

## Validators

Validators are simple functions which take two arguments, the value to be validated,
and arguments provided to the validator (used only by custom validators). A validator
checks the value, and returns `:ok` if it is valid, `{:warn, message}` if it is valid,
but should be brought to the users attention, or `{:error, message}` if it is invalid.

See the moduledoc for `Conform.Schema.Validator` for more details and examples.
"""
[
  extends: [],
  import: [],
  mappings: [
   "ex_to_docker_example.host": [
      commented: false,
      datatype: :binary,
      default: "localhost",
      doc: "Host on which ex_to_docker_example app would wait for connections",
      hidden: false,
      to: "ex_to_docker_example.Elixir.ExToDockerExample.Endpoint.url.host"
    ],
    "ex_to_docker_example.secret_key_base": [
      commented: false,
      datatype: :binary,
      default: "2Q5Xzs1n9p8ZExojG6rvw0J1/1XS/VJSqV7K/7N68jxuax9PcwsP045NigENb24U",
      doc: "Phoenix secret key base",
      hidden: false,
      to: "ex_to_docker_example.Elixir.ExToDockerExample.Endpoint.secret_key_base"
    ],
    "ex_to_docker_example.port": [
      commented: false,
      datatype: :integer,
      default: 4000,
      doc: "Port on which ex_to_docker_example app would listen",
      hidden: false,
      to: "ex_to_docker_example.Elixir.ExToDockerExample.Endpoint.http.port"
    ],
    "ex_to_docker_example.db.adapter": [
      commented: false,
      datatype: :atom,
      default: Ecto.Adapters.Postgres,
      doc: "Database adapter",
      hidden: false,
      to: "ex_to_docker_example.Elixir.ExToDockerExample.Repo.adapter"
    ],
    "ex_to_docker_example.db.username": [
      commented: false,
      datatype: :binary,
      default: "postgres",
      doc: "Database username",
      hidden: false,
      to: "ex_to_docker_example.Elixir.ExToDockerExample.Repo.username"
    ],
    "ex_to_docker_example.db.password": [
      commented: false,
      datatype: :binary,
      default: "postgres",
      doc: "Database password",
      hidden: false,
      to: "ex_to_docker_example.Elixir.ExToDockerExample.Repo.password"
    ],
    "ex_to_docker_example.db.database": [
      commented: false,
      datatype: :binary,
      default: "ex_to_docker_example_prod",
      doc: "Database name",
      hidden: false,
      to: "ex_to_docker_example.Elixir.ExToDockerExample.Repo.database"
    ],
    "ex_to_docker_example.db.hostname": [
      commented: false,
      datatype: :binary,
      default: "localhost",
      doc: "Database host",
      hidden: false,
      to: "ex_to_docker_example.Elixir.ExToDockerExample.Repo.hostname"
    ],
    "ex_to_docker_example.db.pool_size": [
      commented: false,
      datatype: :integer,
      default: 10,
      doc: "Database pool size",
      hidden: false,
      to: "ex_to_docker_example.Elixir.ExToDockerExample.Repo.pool_size"
    ]
  ],
  transforms: [
    "ex_to_docker_example.Elixir.ExToDockerExample.Endpoint.url.host": fn conf ->
      [{_, val}] = Conform.Conf.get(conf, "ex_to_docker_example.Elixir.ExToDockerExample.Endpoint.url.host")
      System.get_env("APP_HOST") || val
    end,
    "ex_to_docker_example.Elixir.ExToDockerExample.Endpoint.http.port": fn conf ->
      [{_, val}] = Conform.Conf.get(conf, "ex_to_docker_example.Elixir.ExToDockerExample.Endpoint.http.port")
      System.get_env("PORT") || val
    end,
    "ex_to_docker_example.Elixir.ExToDockerExample.Endpoint.secret_key_base": fn conf ->
      [{_, val}] = Conform.Conf.get(conf, "ex_to_docker_example.Elixir.ExToDockerExample.Endpoint.secret_key_base")
      System.get_env("PHOENIX_SECRET_KEY_BASE") || val
    end,
    "ex_to_docker_example.Elixir.ExToDockerExample.Repo.username": fn conf ->
      [{_, val}] = Conform.Conf.get(conf, "ex_to_docker_example.Elixir.ExToDockerExample.Repo.username")
      System.get_env("POSTGRES_USER") || val
    end,
    "ex_to_docker_example.Elixir.ExToDockerExample.Repo.password": fn conf ->
      [{_, val}] = Conform.Conf.get(conf, "ex_to_docker_example.Elixir.ExToDockerExample.Repo.password")
      System.get_env("POSTGRES_PASSWORD") || val
    end,
    "ex_to_docker_example.Elixir.ExToDockerExample.Repo.database": fn conf ->
      [{_, val}] = Conform.Conf.get(conf, "ex_to_docker_example.Elixir.ExToDockerExample.Repo.database")
      System.get_env("POSTGRES_DATABASE") || val
    end,
    "ex_to_docker_example.Elixir.ExToDockerExample.Repo.hostname": fn conf ->
      [{_, val}] = Conform.Conf.get(conf, "ex_to_docker_example.Elixir.ExToDockerExample.Repo.hostname")
      System.get_env("POSTGRES_HOST") || val
    end,
  ],
  validators: []
]
