defmodule ExToDockerExample.PageController do
  use ExToDockerExample.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
